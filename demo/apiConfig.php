<?php


return [    
    'v1'=>[		    
        'user'=>'用户',
        'user.get'=>['class'=>'wuyuxifeng\demo\v1\user\Get','auth'=>true], 

        'user.xcx.decode'=>'wuyuxifeng\demo\v1\user\Decode',        
        'user.login'=>\wuyuxifeng\demo\v1\user\Login::class,
        'user.test'=>\wuyuxifeng\demo\v1\user\Test::class,  
        'user.upload'=>\wuyuxifeng\demo\v1\user\Upload::class,  
        'user.upload2'=>\wuyuxifeng\demo\v1\user\Upload2::class,  
        'user.upload3'=>\wuyuxifeng\demo\v1\user\Upload3::class,  
        'user.upload4'=>\wuyuxifeng\demo\v1\user\Upload4::class,  

        'cart'=>'购物车',
        'cart.get'      =>\wuyuxifeng\demo\v1\cart\Get::class,
        'cart.delete'   =>\wuyuxifeng\demo\v1\cart\Delete::class,
        'cart.update'   =>\wuyuxifeng\demo\v1\cart\Update::class,
        'cart.checkout'   =>\wuyuxifeng\demo\v1\cart\Checkout::class,

        'cart2'=>'购物车2',
        'cart2.get'      =>\wuyuxifeng\demo\v1\cart\Get::class,
        'cart2.delete'   =>\wuyuxifeng\demo\v1\cart\Delete::class,
        'cart2.update'   =>\wuyuxifeng\demo\v1\cart\Update::class,
        'cart2.checkout'   =>\wuyuxifeng\demo\v1\cart\Checkout::class,

        'order'=>'订单',
        'theme'=>'主题',
        'member'=>'会员',
        'aftersale'=>'售后',
        'shop'=>'店铺管理',
        'shopmember'=>'店铺用户',
        
    ],
    'v2'=>[		   
        'user'=>'用户',     
        'user.get'=>['class'=>'wuyuxifeng\demo\v2\user\Get','auth'=>true],  
    ],

 
    
 
];